/** 
 * @file tic_tac_toe.c 
 * @author Patrik Paradis
 * @version 2007-07-25
 *
 * @section DESCRIPTION
 * 
 * The point from where the program starts.
 */ 


/****************************** 
*				
*	Namn:	  Patrik Paradis 
*	Dator ID: TDE02018    
*	Datum: 	  2007-07-25	
*				
*******************************/

/* include-direktiv, t.ex. f�r att inkludera stdio.h */
#include <stdio.h>

/*#define DEBUG*/

/* use stdlib.h to get access to random
number generation functionality */
#include <stdlib.h>
#include <time.h>

#include "board.h"

/* egna funktionsprototyper */

static void init_random_number_generation(void);


/** 
 * Function main.
 * 
 * This is where everythings starts.
 *
 * @param void.
 * @return returns an OS related exit code. 
 */
int main(void)
{

	/* a referece to the board */
	board_type_ref board;

	/* create the board */
	board = board_new();
 
	/* initialise random number handling */
	init_random_number_generation() ;

	/* play the game */
	board_play_game(board) ;
	
	/* and delete the board */
	board_delete(board);
	
	/* all is well */
	return 0;
	
} /* end main function */


/* egna funktioner  */

/* initialise random number handling */
static void init_random_number_generation(void)
{

	/* initialise random number generator, using a specified seed,
    resulting in the same number sequence (when calling rand())
    for all executions of the program
    srand(12345); */
    
	/* initialise random number generator, using a time dependent seed,
    resulting in different number sequences (when calling rand())
    for different executions of the program */
    srand(time(NULL));

}
