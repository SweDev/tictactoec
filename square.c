
/**
 * @file square.c 
 * @ingroup SQUARE 
 * @author Patrik Paradis
 * @version 2007-07-25
 *
 * @section DESCRIPTION
 * 
 * Represents the squares of the game board.
 */

/* include-direktiv, t.ex. f�r att inkludera stdio.h */
#include <stdio.h>
#include <stdlib.h>

#include "square.h"

/*#define DEBUG*/


/* egna funktioner  */

/* Places a piece given by piece_type on the square. */
void square_set_piece(/* input arguments related to the board play game */
					  square_type_ref square, 
					  /* input arguments related to the type of piece stored on the square */
					  int piece_type)
{

	#ifdef DEBUG
		printf("3\n") ;
	#endif

	square->free = 0 ;
	square->piece_type = piece_type ;

} /* end square_set_piece function */


/* square_remove_piece: removes a piece given by piece_type from a square */
void square_remove_piece(/* input arguments related to the board play game */
						 square_type_ref square) 
{
	
	#ifdef DEBUG
		printf("4\n") ;
	#endif
  
	/* Here we delete a piece from a square */
	square->free = 1 ;
	square->piece_type = NO_PIECE ;
			   
} /* end square_remove_piece function */


/* square_is_free: checks if a square holds a piece or not */
int square_is_free(/* input arguments related to the board play game */
				   square_type_ref square)
{

	return square->free ;

} /* end square_is_free function */


/* square_new: creates a square */
square_type_ref square_new(void)
{

/* allocate memory */
/* NOTE: square_type, and not square_type_ref is used as argument to malloc */

	square_type_ref square = malloc(sizeof(square_type));

/* initialise fields */
	square->free = 1;
	square->piece_type = NO_PIECE;

/* return a reference (i.e. a pointer) to the square */
	return square;

} /* end square_new function */


/* Deallocates memory for square. */
void square_delete(square_type_ref square)
{
	
	free(square);

} /* end square_delete function */
