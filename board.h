/** 
 * @file board.h
 * @ingroup BOARD
 * @defgroup BOARD BOARD  
 * @author Patrik Paradis
 * @version 2007-07-25
 * 
 *
 * @section DESCRIPTION
 * 
 * Represents the board.
 */

#ifndef BOARD_H
#define BOARD_H

/* include-direktiv, t.ex. f�r att inkludera stdio.h */
#include "square.h"

/* the number of squares */ 
#define N_SQUARES 9

/* egna dataposter */

/* -- data strucure for a 3 by 3 tic-tac-toe board --

   the board is represented by an array, which 
   for the case of a 3 by 3 board represents the 
   squares using the following numbering: 

   -------
   |0|1|2|
   -------
   |3|4|5|
   -------
   |6|7|8|
   -------
*/ 
typedef struct
{
	/*	This array square will hold squares such as 
		CROSS_PIECE and CIRCLE_PIECE or NO_PIECE  */
	square_type_ref square[N_SQUARES] ; 

} board_type ;


/* a pointer data type, acting as a reference to boards */
typedef board_type *board_type_ref;

/* egna funktionsprototyper */

/* board_play_game: plays a game */ 
void board_play_game(/* input arguments related to the board play game */ 
					 board_type_ref board) ;

/* board_new: creates a board */
board_type_ref board_new(void);

/* board_delete: deallocates memory for board */
void board_delete(board_type_ref board);



#endif

