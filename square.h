/** 
 * @file square.h
 * @ingroup SQUARE
 * @defgroup SQUARE SQUARE  
 * @author Patrik Paradis
 * @version 2007-07-25
 * 
 *
 * @section DESCRIPTION
 * 
 * Represents the squares of the game board.
 */

#ifndef SQUARE_H
#define SQUARE_H


/* symbols for tic-tac-toe pieces */ 
#define NO_PIECE -1
#define CROSS_PIECE 0
#define CIRCLE_PIECE 1


/* egna dataposter */

/* data structure for a square */ 
typedef struct
{
	
	/* flag to indicate if a square is free or not */  
    int free ; 
    /*	the type of piece stored on the square if the 
		square is not free, in this case the admissible 
		values are CROSS_PIECE and CIRCLE_PIECE, 
		otherwise the value NO_PIECE is used */ 
    int piece_type ; 

} square_type ;


/* a pointer data type, acting as a reference to squares */
typedef square_type *square_type_ref;


/* egna funktionsprototyper */


/** 
 * Places a piece given by piece_type on the square.
 *
 * @param square input arguments related to the board play game.
 * @param piece_type input arguments related to the type of piece stored on the square.
 * @return void.
 */
void square_set_piece(square_type_ref square,
		      int piece_type) ;
	

/** 
 * Removes a piece given by piece_type from a square.
 *
 * @param square input arguments related to the board play game.
 * @return void.
 */
void square_remove_piece(square_type_ref square);
					  

/** 
 * Checks if a square holds a piece or not.
 *
 * @param square input arguments related to the board play game.
 * @return 1-Square is not free or 0-Square is free.
 */
int square_is_free(square_type_ref square);


/** 
 * Creates a square.
 *
 * @param void.
 * @return a newly created square.
 */
square_type_ref square_new(void);


/** 
 * Deletes square.
 *
 * @param a square.
 * @return void.
 */
void square_delete(square_type_ref square);


#endif
