/** 
 * @file board.c
 * @ingroup BOARD 
 * @author Patrik Paradis
 * @version 2007-07-25
 *
 * @section DESCRIPTION
 * 
 * Represents the board.
 */


/* include-direktiv, t.ex. f�r att inkludera stdio.h */
#include <stdio.h>

/* use stdlib.h to get access to random
number generation functionality */
#include <stdlib.h>
#include <time.h>

#include "board.h"
#include "square.h"

/*#define DEBUG*/


/* egna funktionsprototyper av hj�lpfunktionerna */

/* board_make_move: the crosses or the circles makes a move, i.e. a human move or a computer move */
static void board_make_move(/* input arguments related to the board play game */
			    board_type_ref board, 
       			    /* input arguments related to the type of piece stored on the square */
			    int piece_type,
			    /* input arguments related to the amount of times the board_play_game runs */
			    int n_moves) ;

/* board_is_blocking_piece: Check if a surtain piece type at a given square is a piece that
is placed as a blocking piece between two opposing pieces that 	
keeps the opposing piece from creating a win pattern */
static int board_is_blocking_piece(/* input arguments related to the board play game */
								   board_type_ref board,
							       /* input argument that indicate a specific square on the board */
							       int square_index, 
							       /* input argument that indicate a surtain type of piece on a square */
							       int piece_type) ;

/* board_display: display the board before starting */
static void board_display(/* input arguments related to the board play game */
						  board_type_ref board) ;

/* board_has_win_pattern: check if the human or computer has won */
static int board_has_win_pattern(/* input arguments related to the board play game */
					             board_type_ref board, 
					             /* input argument that indicate a surtain type of piece on a square */
					             int piece_type) ;

/* board_set_piece_random: places a piece at a
randomly chosen position, except at the position no_set_index,
a parameter which is used to prevent the computer from placing
the piece at the same square as it was previously taken from */
static void board_set_piece_random(/* input arguments related to the board play game */
								   board_type_ref board, 
							       /* input argument that indicate a surtain type of piece on a square */
							       int piece_type,
							       /* input argument that indicate a specific square on the board where a piece 
							       can not be placed */
							       int no_set_index) ;

/* board_delete_random_circle_piece: delete a circle piece from a
randomly chosen square */
static int board_delete_random_circle_piece(/* input arguments related to the board play game */
									board_type_ref board, 
									/* input argument that indicate a surtain type of piece on a square */
									int piece_type) ;

/* cross_piece_make_move: the human makes a move */
static void cross_piece_make_move(/* input arguments related to the board play game */
	     						  board_type_ref board, 
								  /* input arguments related to the amount of times the board_play_game runs */
								  int n_moves) ;

/* circle_piece_make_move: the computer makes a move */
static void circle_piece_make_move(/* input arguments related to the board play game */
					               board_type_ref board, 
				                   /* input arguments related to the amount of times the board_play_game runs */
					               int n_moves) ;

/* circle_piece_make_win_move: the computer makes a winning move */
static int circle_piece_make_win_move(/* input arguments related to the board play game */
									  board_type_ref board, 
							          /* input arguments related to the amount of times the board_play_game runs */
									  int n_moves) ;

/* circle_piece_make_block_move: the computer makes a move to block the human from winning */
static int circle_piece_make_blocking_move(/* input arguments related to the board play game */
										   board_type_ref board, 
										   /* input arguments related to the amount of times the board_play_game runs */
										   int n_moves) ;

/* circle_piece_make_non_stratigic_move: the computer does not follow any stratagy and makes a random move */
static void circle_piece_make_non_stratigic_move(/* input arguments related to the board play game */
												 board_type_ref board, 
												 /* input arguments related to the amount of times the board_play_game runs */
												 int n_moves) ;


/* egna funktioner  */

/**
 * Plays a game. 
 *
 * @param board the game board
 * @return void
 */ 
void board_play_game(/* input arguments related to the board play game */
					 board_type_ref board)
{

	/* number of moves */ 
    int n_moves = 0 ; 
   
	/* prints the message "Tic-tac-toe game" to the user */
    printf("Tic-tac-toe game") ;

    /* display the board before starting */ 
    board_display(board);  

    /* game loop, which is exited using break */ 
    while (1)
    {

		/* n_moves get incrementet every time the human and the computer makes a move */
		n_moves++ ;
		
		#ifdef DEBUG 
			printf("antal k�rda rundor:%d\n", n_moves) ;
		#endif

		/* board_make_move: the crosses makes a move, i.e. a human move */ 
		board_make_move(board, CROSS_PIECE, n_moves) ; 

        /* present result of human move */ 
        printf("your move:\n"); 
        board_display(board) ; 

		/* check if the human has won */ 
        if (board_has_win_pattern(board, CROSS_PIECE))
        {
            
			printf("The CROSSES won, after %d moves\n", n_moves) ;
            break ; 

        } /* end if statement */

        /* the circles makes a move, i.e. a computerized move */ 
        board_make_move(board, CIRCLE_PIECE, n_moves) ; 

        /* present result of computer's move */ 
        printf("computer's move:\n") ;	
        board_display(board) ; 

		/* check if the computer has won */
        if (board_has_win_pattern(board, CIRCLE_PIECE))
		{
          
			printf("The CIRCLES won, after %d moves\n", n_moves) ;
			break ;

		} /* end if statement*/
    
	} /* end while statement */

} /* end board_play_game function */


/* board_make_move: the crosses or the circles makes a move, i.e. a human move or a computer move */ 
static void board_make_move(/* input arguments related to the board play game */
							board_type_ref board, 
							/* input arguments related to the type of piece stored on the square */
							int piece_type,
							/* input arguments related to the amount of times the board_play_game runs */
							int n_moves)
					 
{
  
	if(piece_type == CROSS_PIECE)
	{

		#ifdef DEBUG
			printf("1\n") ;
		#endif
	
		/* the human makes a move */
		cross_piece_make_move(board,n_moves) ;

	} /* end if statement */

	if(piece_type == CIRCLE_PIECE) 
	{
	
		#ifdef DEBUG
			printf("2\n") ;
		#endif

		/* the computer makes a move */
		circle_piece_make_move(board,n_moves) ;
	
	} /* end if statement */

} /* end board_make_move function */


/* board_is_blocking_piece: Check if a surtain piece type at a given square is a piece that
is placed as a blocking piece between two opposing pieces that 	
keeps the opposing piece from creating a win pattern */
static int board_is_blocking_piece(/* input arguments related to the board play game */
								   board_type_ref board,
							       /* input argument that indicate a specific square on the board */
							       int square_index, 
							       /* input argument that indicate a surtain type of piece on a square */
							       int piece_type)  
{

	/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
	when 3 the same pieces are placed horozontally, vertically or diogonally */
	int magic_squares[9] = {4,3,8,9,5,1,2,7,6} ; 
	
	/* circle_piece_index_array[i] is an array that holds the exact index/location of a circle piece on the board */
	int circle_piece_index_array[3] = {0} ; 
	
	/* cross_piece_index_array[i] is an array that holds the exact index/location of a cross piece on the board */
	int cross_piece_index_array[3] = {0} ;
	
	/* cross_win_sum and circle_win_sum hold a number determined by magic_squares[i] and the position where
	the pieces are placed at the board */
	int cross_win_sum = 0, circle_win_sum = 0 ;
		
    /* n_circle_piece_type and n_cross_piece_type both hold a number that is determined by the amount of pieces 
	placed on the board of a surtain type */
	int n_circle_piece_type = 0, n_cross_piece_type = 0 ;

	/* Indexes used for iterating array values */
	int x = 0, y = 0, i ; 

	/* for statement checks if a square holds a specific piece type and if it does 
	we store the exact location of that piece */
	for(i = 0 ; i < N_SQUARES ; i++)
	{
		
		if(board->square[i]->piece_type == CIRCLE_PIECE)
		{	
			
			circle_piece_index_array[x] = i ;
			x++ ;
			
		} /* end if statement*/

		if(board->square[i]->piece_type == CROSS_PIECE)
		{
	    
			cross_piece_index_array[y] = i ;
			y++ ;

		} /* end if statement*/

	} /* end for statement*/

	/* if statements checks if a square at the given location, square_index, is placed as a blocking piece and if it is
	   the board_is_blocking_piece() function returns 1 */
	if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[1]] + magic_squares[square_index] ) == 15 ) )
	{
			
		#ifdef DEBUG
			printf("a %d \n", square_index) ;
		#endif
		
		return 1 ;
		
	} /* end if statement */
	else if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[1]] + magic_squares[square_index] ) == 15 ) )
	{
		
		#ifdef DEBUG
			printf("b %d \n", square_index) ;
		#endif

		return 1 ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[1]] + magic_squares[square_index] ) == 15 ) )
	{
		
		#ifdef DEBUG
			printf("c %d \n", square_index) ;
		#endif

		return 1 ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{
		
		#ifdef DEBUG
			printf("d %d \n", square_index) ;
		#endif

		return 1 ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{
		
		#ifdef DEBUG
			printf("e %d \n", square_index) ;
		#endif

		return 1 ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{
		
		#ifdef DEBUG
			printf("f %d \n", square_index) ;
		#endif

		return 1 ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[1]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{
		
		#ifdef DEBUG
			printf("g %d \n", square_index) ;
		#endif

		return 1 ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[1]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{
		
		#ifdef DEBUG
			printf("h %d \n", square_index) ;
		#endif

		return 1 ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[1]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{
		
		#ifdef DEBUG
			printf("i %d \n", square_index) ;
		#endif

		return 1 ;
		   
	} /* end else if statement */
	else
	{

		#ifdef DEBUG
			printf("j %d \n", square_index) ;
		#endif

		return 0 ;

	} /* end else statement */

} /* end board_is_blocking_piece function */


/* board_display: display the board before starting */
static void board_display(/* input arguments related to the board play game*/
						  board_type_ref board)
{

	/* loop counter */
	int i ;

	/* array[i] will hold the characters that will be diplayed on the game board instead of numbers */
	char array[9] = {' '} ;
  
	/* We check square[i] with if statments what type of pieces it holds and 
	use the array[i] to store characters at the same squares.These characters will 
	be printed on the game board instead of non user friendly numbers that are stored
	in square[i]. The idea is to print characters and not integers */
	for(i = 0 ; i < N_SQUARES ; i++)
	{
   	
		if(board->square[i]->piece_type == NO_PIECE)
			array[i] = ' ' ;

		if(board->square[i]->piece_type == CROSS_PIECE)
			array[i] = 'X' ; 

		if(board->square[i]->piece_type == CIRCLE_PIECE)
			array[i] = 'O' ;  

	} /* end for-loop */

	/* Now lets display what square[i] holds by the use of array[i] */
	printf("\n") ;
	printf("-------\n") ;
	printf("|%c|%c|%c|\n", array[0],array[1],array[2]) ; 
	printf("-------\n") ;
	printf("|%c|%c|%c|\n", array[3],array[4],array[5]) ;
	printf("-------\n") ;
	printf("|%c|%c|%c|\n", array[6],array[7],array[8]) ;
	printf("-------\n") ;

} /* end board_display function */


/* board_has_win_pattern: check if the human or computer has won */
static int board_has_win_pattern(/* input arguments related to the board play game */
					      board_type_ref board, 
						  /* input arguments related to the type of piece stored on the square */
					      int board_piece)
{
    
	/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
	when 3 the same pieces are placed horozontally, vertically or diogonally */
	int magic_squares[9] = {4,3,8,9,5,1,2,7,6} ;
	
	/* win_sum hold a number determined by magic_squares[i] and the position where
	the pieces are placed at the board,
	n_piece_type hold a number that is determined by the amount of pieces placed on 
	the board of a surtain type */
	int win_sum = 0, i, n_piece_type = 0 ;
    
	/* for statement checks if a square holds a specific piece type and if it does 
	the variabel win_sum stores a number that must be 15 for that particular piece 
	type to win the game */
	for(i = 0 ; i < N_SQUARES ; i++)
	{
		
		if(board->square[i]->piece_type == board_piece)
		{
	    
			win_sum += magic_squares[i] ;
			n_piece_type += 1 ;

		} /* end if statment*/
	
	} /* end for loop */

	/* if statement checks if a particular piece type has won. The piece type need to have the win_sum to hold the value,
	15 and there has to be exactly 3 pieces of a curtain piece type on the board.
	If a surtain piece type has a win pattern of the board this function board_has_win_pattern() returns 1 
	else it returns 0 */
    if( ( win_sum == 15 ) && ( n_piece_type == 3 ) )
      return 1 ;
	else
	  return 0 ;

} /* end board_has_win_pattern function */


/* board_set_piece_random: places a piece at a
randomly chosen position, except at the position no_set_index,
a parameter which is used to prevent the computer from placing
the piece at the same square as it was previously taken from */
static void board_set_piece_random(/* input arguments related to the board play game */
								   board_type_ref board, 
								   /* input argument that indicate a surtain type of piece on a square */
								   int piece_type,
								   /* input argument that indicate a specific square on the board where a piece 
								   can not be placed */
								   int no_set_index)
{
  
  /* maximum number of iterations */
  const int max_n_iter = 100 ;
  
  /* actual number of iterations */
  int n_iter ;

  /* flag to indicate that a free place is found */
  int found ;
  
  /* the square index where the piece shall be placed */
  int square_index ;
  
  /* no iterations done */
  n_iter = 0 ;
  
  /* the square is not found */
  found = 0 ;
  
  /* as long as a square is not found and not too many iterations
  are performed */
  while (!found && n_iter < max_n_iter)
  {
    
	/* one more iteration */
    n_iter++ ;
    
	/* select a random square, i.e. select a number between 0 and
    N_SQUARES-1 */
    square_index = rand() % N_SQUARES ;
    
	/* check if the index is allowed and the square is free,
    and if this is the case, the piece is placed on the square */
    found = (square_index != no_set_index) && (square_is_free(board->square[square_index])) ;
    
	if (found)
	{
      
      square_set_piece(board->square[square_index], piece_type) ;
	
	} /* end if-statement */
  
  } /* end while-statement */
  
  if (!found)
  {
  
    printf("ERROR: could not place piece\n");
  
  } /* end if-statement */

} /* end board_set_piece_random function */


/* board_delete_random_circle_piece: delete a circle piece from a
randomly chosen square */
static int board_delete_random_circle_piece(/* input arguments related to the board play game */
											board_type_ref board, 
											/* input argument that indicate a surtain type of piece on a square */
											int piece_type) 
{
  
  /* maximum number of iterations */
  const int max_n_iter = 100 ;
  
  /* actual number of iterations */
  int n_iter ;

  /* flag to indicate that a free place is found */
  int found ;
  
  /* the square index where the piece shall be removed from */
  int square_index ;
  
  /* no iterations done */
  n_iter = 0 ;
  
  /* the square is not found */
  found = 0 ;
  
  /* as long as a square is not found and not too many iterations
  are performed */
  while (!found && n_iter < max_n_iter)
  {
    
	/* one more iteration */
    n_iter++ ;
    
	/* select a random square, i.e. select a number between 0 and
    N_SQUARES-1 */
    square_index = rand() % N_SQUARES ;
    
	/* check if the index is allowed and the square is a CIRCLE_PIECE,
    and if this is the case, the piece is moved from the square */
    found = (board->square[square_index]->piece_type == piece_type) && (board_is_blocking_piece(board,square_index, piece_type) != 1) ;

	//(square_index != no_set_index) && (square_is_free(&board->square[square_index]))
    
	if (found)
	{
      
      /* Here we delete the CIRCLE_PIECE at the given position a, 
      given by the human player */
	  
	  square_remove_piece(board->square[square_index]) ;
	  
	} /* end if-statement */
  
  } /* end while-statement */
  
  if (!found)
  {
  
    printf("ERROR: could not move piece\n");
  
  } /* end if-statement */


  return square_index ;

} /* end board_delete_random_circle_piece */


/* cross_piece_make_move: the human makes a move */
static void cross_piece_make_move(/* input arguments related to the board play game */
	     						  board_type_ref board, 
								  /* input arguments related to the amount of times the board_play_game runs */
								  int n_moves)
{

	/* variabels used to store input from the user */
	int i = 0, a = 0, b = 0 ;
   
	/* Check how many moves has been made so far in the game run */
	if(n_moves>3)
	  {

		do
		{
          
	      /* Ask the user "from where to where? two numbers in the range [0-8]: " 
	      and store the input into the variables a and b */
		  printf("from where to where? two numbers in the range [0-8]: ") ;
	      scanf("%d %d",&a,&b) ;

		} while (!(square_is_free(board->square[b]) == 1)) ; /* end while statement */

		/* Here we delete the CROSS_PIECE at the given position a, 
        given by the human player */
		
		square_remove_piece(board->square[a]) ;
				
		/* Here we actually place the CROSS_PIECE at the given position i, 
        given by the human player */
		square_set_piece(board->square[b],CROSS_PIECE) ;
	  
	  } /* end if statement */
	  	  
   if(n_moves <= 3)
   {
	
     do
	 {
      
	    /* Ask the user "to where? a number in the range [0-8]: " 
	    and store the input into the variable i */
        printf("to where? a number in the range [0-8]: ") ;
        scanf("%d",&i) ;
	  
	 } while(!(square_is_free(board->square[i]) == 1)) ; /* end while statment */

	   /* Here we actually place the CROSS_PIECE at the given position i, 
         given by the human player */
       square_set_piece(board->square[i],CROSS_PIECE) ;

   }

}


/* circle_piece_make_move: the computer makes a move */
static void circle_piece_make_move(/* input arguments related to the board play game */
								   board_type_ref board, 
								   /* input arguments related to the amount of times the board_play_game runs */
								   int n_moves) 
{
  
	/* circle_piece_made_win_move will indicate if the computer made a stratigic move to win the game
	circle_piece_made_blocking_move will indicate if the computer made a stratigic move to block the 
	human from winning, no_set_index is the square index where a piece can not be placed at */
	int circle_piece_made_win_move = 0, circle_piece_made_blocking_move = 0 , no_set_index = 0 ;
  
	/*circle_piece_make_stratigic_move(board,n_moves) ;*/

	#ifdef DEBUG
		printf("5\n") ;
	#endif
	
	/* The computer makes a winning move */
	circle_piece_made_win_move = circle_piece_make_win_move(board, n_moves) ;

	/* If the computer has not made a win move then it does a blocking move */
	if(circle_piece_made_win_move == 0)
	{

		#ifdef DEBUG
			printf("6\n") ;
		#endif
		
		/* The computer did not make a win move so it does a blocking move instead */
		circle_piece_made_blocking_move = circle_piece_make_blocking_move(board, n_moves) ;

		/* If the computer deos not do a win move or blocking move it makes a random move */
		if(circle_piece_made_blocking_move == 0 && circle_piece_made_win_move == 0)
		{
		
			#ifdef DEBUG
				printf("7\n") ;
			#endif
			
			/* The computer makes a random move */
			circle_piece_make_non_stratigic_move(board, n_moves) ;

		} /* end if-statement */

	} /* end if-statement */
	 
} /* end circle_piece_make_move function */


/* circle_piece_make_win_move: the computer makes a winning move */
static int circle_piece_make_win_move(/* input arguments related to the board play game */
									  board_type_ref board, 
									  /* input arguments related to the amount of times the board_play_game runs */
									  int n_moves)
{
	
	/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
	when 3 the same pieces are placed horozontally, vertically or diogonally */
	int magic_squares[9] = {4,3,8,9,5,1,2,7,6} ; 
	
	/* circle_piece_index_array[i] is an array that holds the exact index/location of a circle piece */
	int circle_piece_index_array[3] = {0} ; 
	
	/* cross_piece_index_array[i] is an array that holds the exact index/location of a cross piece */
	int cross_piece_index_array[3] = {0} ;
	
	/* Variabels that hold the sum of the magic square values */
	int cross_win_sum = 0, circle_win_sum = 0 ;
		
    /* Variabels that hold the the amount of pieces that exists on the game board */
	int n_circle_piece_type = 0, n_cross_piece_type = 0 ; 
	
	/* A variabel that holds the index/location where a piece will not be placed */
	int no_set_index = 0 ; 
		 	
	/* A variabel used to keep track if a square has recently been placed on the game board */
	int square_piece_set = 0 ; 
	
    /* Indexes used for iterating array values */
	int x = 0, y = 0, i ; 
	
	#ifdef DEBUG
		printf("8\n") ;
	#endif


	/* check if the computer have made less or exactly 3 moves and if so then do something */
	if(n_moves <= 3)
	{

		#ifdef DEBUG
			printf("9\n") ;
		#endif

		/* Checks what kind of piece type that exists on the game board, when a circle piece is found
		add some value to the variable circle_win_sum and keep track on how many
		circle pieces we have on the board */
		for(i = 0 ; i < N_SQUARES ; i++)
		{
			
			/* If we hava found a circle piece on the game board do something */
			if(board->square[i]->piece_type == CIRCLE_PIECE)
			{	
	    
				circle_win_sum += magic_squares[i] ;
				n_circle_piece_type += 1 ;
		
				#ifdef DEBUG
					printf("10\n") ;
				#endif

			} /* end if-statement */

		} /* end for-statement */	
        
		#ifdef DEBUG
		printf("antal O:%d\n", n_circle_piece_type ) ;
		printf("antal X:%d\n", n_cross_piece_type ) ;
		#endif

		/* The for-statement checks how if the circle pieces are placad in a specific way that can give a win pattern on the game board */
		for(i = 0 ; i < N_SQUARES ; i++)
		{
    
			/*printf("\n21\n") ;*/
			if( (magic_squares[i] == (15-circle_win_sum)) && (square_is_free(board->square[i])) && (square_piece_set != 1) && (n_circle_piece_type == 2) )
			{
		       
				/* Place a circle piece on the board to create a win pattern */
				square_set_piece(board->square[i], CIRCLE_PIECE) ;
				square_piece_set = 1 ; 
				
				#ifdef DEBUG
					printf("11\n") ;
				#endif

			} /* end if statement */
	
		}  /* end for-statement */

	} /* end if-statement */


	/* check if the computer have made more then 3 moves and if so then do something */
	if(n_moves > 3)
	{

		/* For statement checks what kind of piece type that exists on the game board, when a circle piece is found
		it's index/location gets stored in the circle_piece_index_array[i] and keep track on how many cross 
		pieces we have on the board */
		for(i = 0 ; i < N_SQUARES ; i++)
		{
			
			/* If we hava found a circle piece on the game board do something */
			if(board->square[i]->piece_type == CIRCLE_PIECE)
			{	
				
				circle_piece_index_array[x] = i ;
				n_circle_piece_type += 1 ;
				x++ ;
				
				#ifdef DEBUG
					printf("12\n") ;
				#endif

			} /* end if-statement*/

		} /* end for-statement */
	
		#ifdef DEBUG
			printf("%d %d %d\n", circle_piece_index_array[0], circle_piece_index_array[1], circle_piece_index_array[2]) ;
			printf("%d %d %d\n", cross_piece_index_array[0], cross_piece_index_array[1], cross_piece_index_array[2]) ;
			printf("\n30\n") ;
		#endif
		
		/* The for-statement checks how the pieces are placad at the game board and then uses a stratagy for the computer */
		for(i = 0 ; i < N_SQUARES ; i++)
		{

			if( (magic_squares[i] == (15-(magic_squares[circle_piece_index_array[0]] + magic_squares[circle_piece_index_array[1]]))) && (square_is_free(board->square[i]) ) && (n_circle_piece_type == 3) && (square_piece_set != 1) )
			{
			   
				/* We remove a CIRCLE_PIECE and place it where it will create a win pattern */
			   
				square_remove_piece(board->square[circle_piece_index_array[2]]) ;
				   			   
				square_set_piece(board->square[i], CIRCLE_PIECE) ;
				square_piece_set = 1 ;
				
				#ifdef DEBUG
					printf("13\n") ;
				#endif
		   
		   } /* end if-statement */
		   else if( (magic_squares[i] == (15-(magic_squares[circle_piece_index_array[0]] + magic_squares[circle_piece_index_array[2]]))) && (square_is_free(board->square[i]) ) && (n_circle_piece_type == 3) && (square_piece_set != 1) )
		   {
			   
			   /* We remove a CIRCLE_PIECE and place it where it will create a win pattern */

			   square_remove_piece(board->square[circle_piece_index_array[1]]) ;
			   
			   square_set_piece(board->square[i], CIRCLE_PIECE) ;
		       square_piece_set = 1 ;
			   
			   #ifdef DEBUG
			       printf("14\n") ;
			   #endif
		   
		   } /* end else if-statement */
		   else if( (magic_squares[i] == (15-(magic_squares[circle_piece_index_array[1]] + magic_squares[circle_piece_index_array[2]]))) && (square_is_free(board->square[i]) ) && (n_circle_piece_type == 3) && (square_piece_set != 1) )
		   {
			   
			   /* We remove a CIRCLE_PIECE and place it where it will create a win pattern */
			   
			   square_remove_piece(board->square[circle_piece_index_array[0]]) ;
			   
			   square_set_piece(board->square[i], CIRCLE_PIECE) ;
		       square_piece_set = 1 ;
			   
			   #ifdef DEBUG
			       printf("15\n") ;
			   #endif

		   } /* end else if-statement */

		} /* end for-statement*/

	} /* end if-statement*/

	#ifdef DEBUG
	printf("win move made: %d \n", square_piece_set) ;
    #endif

	return square_piece_set ;

} /* end circle_piece_make_win_move function */


/* circle_piece_make_block_move: the computer makes a move to block the human from winning */
static int circle_piece_make_blocking_move(/* input arguments related to the board play game */
										   board_type_ref board, 
										   /* input arguments related to the amount of times the board_play_game runs */
										   int n_moves) 
{

	/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
	when 3 the same pieces are placed horozontally, vertically or diogonally */
	int magic_squares[9] = {4,3,8,9,5,1,2,7,6} ; 
	
	/* circle_piece_index_array[i] is an array that holds the exact index/location of a circle piece */
	int circle_piece_index_array[3] = {0} ; 
	
	/* cross_piece_index_array[i] is an array that holds the exact index/location of a cross piece */
	int cross_piece_index_array[3] = {0} ;
	
	/* Variabels that hold the sum of the magic square values */
	int cross_win_sum = 0, circle_win_sum = 0 ;
		
    /* Variabels that hold the the amount of pieces that exists on the game board */
	int n_circle_piece_type = 0, n_cross_piece_type = 0 ; 
	
	/* A variabel that holds the index/location where a piece will not be placed */
	int no_set_index = 0 ; 
		 	
	/* A variabel used to keep track if a square has recently been placed on the game board */
	int square_piece_set = 0 ; 
	
    /* Indexes used for iterating array values */
	int x = 0, y = 0, i ; 

	#ifdef DEBUG
		printf("16\n") ;
	#endif

	/* check if the computer have made less or exactly 3 moves and if so then do something */
	if(n_moves <= 3)
	{

    /* For statement checks what kind of piece type that exists on the game board, when a cross piece is found
	it's index/location gets stored in the cross_piece_index_array[i], add some value to the variable cross_win_sum 
	and keep track on how many cross pieces we have on the board */
	for(i = 0 ; i < N_SQUARES ; i++)
	{
		
		/* If we hava found a cross piece on the game board do something */
		if(board->square[i]->piece_type == CROSS_PIECE)
		{
			
			cross_win_sum += magic_squares[i] ;
			n_cross_piece_type += 1 ;
	    
			cross_piece_index_array[y] = i ;
			y++ ;
		
			#ifdef DEBUG
				printf("17\n") ;
			#endif

		} /* end if-statement */

	} /* end for-statement */
       
		#ifdef DEBUG 
			printf("antal O:%d\n", n_circle_piece_type ) ;
			printf("antal X:%d\n", n_cross_piece_type ) ;
		#endif

	/* The for-statement checks how the pieces are placad at the game board and then uses a stratagy for the computer */
	for(i = 0 ; i < N_SQUARES ; i++)
	{
	
		if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[1]]))) && (square_is_free(board->square[i])) && (n_cross_piece_type == 3) && (square_piece_set != 1))
		{
			
			/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
			from creating a win pattern */
			square_set_piece(board->square[i], CIRCLE_PIECE) ;
			square_piece_set = 1 ;
		
			#ifdef DEBUG
				printf("18\n") ;
			#endif

		} /* end if-statement */
		else if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[2]]))) && (square_is_free(board->square[i]) ) && (n_cross_piece_type == 3) && (square_piece_set != 1) )
		{
			
			/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
			from creating a win pattern */
			square_set_piece(board->square[i], CIRCLE_PIECE) ;
			square_piece_set = 1 ;
		
			#ifdef DEBUG
				printf("19\n") ;
			#endif

		} /* end else if-statement */
		else if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[1]] + magic_squares[cross_piece_index_array[2]]))) && (square_is_free(board->square[i]) ) && (n_cross_piece_type == 3) && (square_piece_set != 1) )
		{
			
			/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
			from creating a win pattern */
			square_set_piece(board->square[i], CIRCLE_PIECE) ;
			square_piece_set = 1 ;
		
			#ifdef DEBUG
				printf("20\n") ;
			#endif
		   
		} /* end else if-statement */
		else if( (magic_squares[i] == (15-cross_win_sum)) && (n_cross_piece_type == 2) && (square_is_free(board->square[i])) && (square_piece_set != 1) )
		{
			
			/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
			from creating a win pattern */
			square_set_piece(board->square[i], CIRCLE_PIECE) ;
			square_piece_set = 1 ;
		
			#ifdef DEBUG
				printf("21\n") ;
			#endif

		} /* end else if-statement */

	} /* end for-loop statement */

	} /* end if-statement */ 

	/* check if the computer have made more then 3 moves and if so then do something */
	if(n_moves > 3)
	{

		/* For statement checks what kind of piece type that exists on the game board, when a cross piece is found
		it's index/location gets stored in the cross_piece_index_array[i], keep track on how many cross pieces we 
		have on the board */
		for(i = 0 ; i < N_SQUARES ; i++)
		{
			/* If we hava found a cross piece on the game board do something */
			if(board->square[i]->piece_type == CROSS_PIECE)
			{	
	      
				cross_piece_index_array[y] = i ;
				n_cross_piece_type += 1 ;
				y++ ;
				
				#ifdef DEBUG
					printf("22\n") ;
				#endif

			} /* end if-statement */

		} /* end for-statement */

		/* The for-statement checks how the pieces are placad at the game board and then uses a stratagy for the computer */
		for(i = 0 ; i < N_SQUARES ; i++)
		{

			if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[1]]))) && (square_is_free(board->square[i])) && (n_cross_piece_type == 3) && (square_piece_set != 1))
			{
			   
				/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
				from creating a win pattern */
				no_set_index = board_delete_random_circle_piece(board, CIRCLE_PIECE) ;
		
				square_set_piece(board->square[i], CIRCLE_PIECE) ;
				square_piece_set = 1 ;
			
				#ifdef DEBUG
					printf("23\n") ;
				#endif

			} /* end if-statement */
			else if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[2]]))) && (square_is_free(board->square[i]) ) && (n_cross_piece_type == 3) && (square_piece_set != 1) )
			{
			   
				/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
				from creating a win pattern */
				no_set_index = board_delete_random_circle_piece(board, CIRCLE_PIECE) ;
		
				square_set_piece(board->square[i], CIRCLE_PIECE) ;
				square_piece_set = 1 ;
			
				#ifdef DEBUG
					printf("24\n") ;
				#endif

			} /* end else if-statement */
			else if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[1]] + magic_squares[cross_piece_index_array[2]]))) && (square_is_free(board->square[i]) ) && (n_cross_piece_type == 3) && (square_piece_set != 1) )
			{
			   
				/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
				from creating a win pattern */
				no_set_index = board_delete_random_circle_piece(board, CIRCLE_PIECE) ;
		
				square_set_piece(board->square[i], CIRCLE_PIECE) ;
				square_piece_set = 1 ;			   
			
				#ifdef DEBUG
					printf("25\n") ;
				#endif
		   
			} /* end else if-statement */

		} /* end for-loop statement */

	} /* end if-statement */

	#ifdef DEBUG
	printf("block move made: %d \n", square_piece_set) ;
    #endif
 	
	return square_piece_set ;

} /* end circle_piece_make_blocking_move function */


/* The computer does not follow any stratagy and makes a random move. */
static void circle_piece_make_non_stratigic_move(/* input arguments related to the board play game */
												 board_type_ref board, 
												 /* input arguments related to the amount of times the board_play_game runs */
												 int n_moves) 
{

	/* A variabel that holds the index/location where a piece will not be placed */
	int no_set_index = 0 ; 
	
	/* check if the computer have made less or exactly 3 moves and if so then do something */
	if(n_moves <= 3)
	{
			
		#ifdef DEBUG
			printf("26\n") ;
		#endif
		
		/* place a CIRCLE_PIECE at a random place at the board that is not the location of no_set_index */
		board_set_piece_random(board, CIRCLE_PIECE, no_set_index) ;
	
	} /* end if-statement */

	/* check if the computer have made more then 3 moves and if so then do something */
	if(n_moves > 3)
	{
      
		/* remove a CIRCLE_PIECE at a random place at the board */
		no_set_index = board_delete_random_circle_piece(board, CIRCLE_PIECE) ;
		
		/* place a CIRCLE_PIECE at at a random place at the board */
		board_set_piece_random(board, CIRCLE_PIECE, no_set_index) ;
	    
		#ifdef DEBUG
			printf("27\n") ;
		#endif

	} /* end if-statement*/

} /* end circle_piece_make_non_stratigic_move function */


/**
 * Creates a board.
 * 
 * @param void
 * @return board return a reference (i.e. a pointer) to the board
 */
board_type_ref board_new(void)
{

	/* a reference to the created board */
	board_type_ref board;

	/* loop counter */
	int i;

	/* allocate memory */
	/* NOTE: board_type, and not board_type_ref is used as argument to malloc */
	board = (board_type_ref) malloc(sizeof(board_type));

	/* initialise all squares */
	for (i = 0; i < N_SQUARES; i++)
	{
	
		board->square[i] = square_new();

	}
	
	/* return a reference (i.e. a pointer) to the board */
	return board;

} /* end board_new function */

/** 
 * Deallocates memory for board.
 *
 * @param board the game board
 * @return void 
 */
void board_delete(board_type_ref board)
{

	int i;
	/* delete all squares */
	for (i = 0; i < N_SQUARES; i++)
	{
	
		square_delete(board->square[i]);
	
	}
	
	/* delete the board */
	free(board);

} /* end board_delete function */

